#ifndef DOCXSTRUCT_H
#define DOCXSTRUCT_H

#include "docxenums.h"
#include <string>

namespace docx
{

/*!
 * \brief Настройки отступов
 */
struct Margins
{
    double leftMargin = 0;   ///< Отступ слева
    double rightMargin = 0;  ///< Отступ справа
    double topMargin = 6;    ///< Отступ сверху
    double bottomMargin = 0; ///< Отступ снизу
};

/*!
 * \brief Настройки шрифта
 */
struct Font
{
    bool bold = false;       ///< жирность
    bool italic = false;     ///< курсив
    bool underlined = false; ///< подчеркнуто
    int fontSize = 12;       ///< размер кегеля
};

/*!
 * \brief Стиль, применяемый к тексту
 *
 * Отдельный стиль на текст, на заголовки, на подписи, ...
 */
struct Style
{
    std::string style(const std::string& objectId = {});
    std::string name;                                              ///< Имя стиля в Word
    double lineSpacing = 1.25;                                     ///< Междустрочный интервал
    double paragraphMargin = 1.25;                                 ///< Абзацный отступ
    Margins margins;                                               ///< Отступы текста см. Margins
    ReferenceLevel referenceLevel = ReferenceLevel::NoRef;         ///< см. ReferenceLevel
    TextAlignment textAlignment = TextAlignment::AlignAcrossWidth; ///< см. TextAlignment
    Font font;                                                     ///< см. Font
};

enum NumerableType
{
    NotNumerable,
    Numerable
};

struct FormulaSettings
{
    FormulaSettings(NumerableType numerable, int formulaPartFlags, std::string id = {});
    FormulaSettings(NumerableType numerable = NotNumerable, std::string id = {});
    NumerableType numerable; ///< Следует ли нумеровать формулу
    int formulaPartFlags;    ///< это число следует собирать из элементов FormulaBlocks
    std::string id;          ///< Идентификатор формулы для ссылок
};

using Postfix = std::string;

//struct AdditionalText
//{
//    AdditionalText() = default;
//    AdditionalText(AdditionalTextType type, std::string text, bool spaceDelim = true);
//    AdditionalTextType type = Postfix;
//    std::string text;
//    bool spaceDelim = true;
//};

//Вставляемая картинка
struct Picture
{
    std::string url;
    int height = 0;
    int width = 0;
};

struct Source
{
    std::string id;
    std::string content;
};

} // namespace docx

#endif // DOCXSTRUCT_H

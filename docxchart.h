#pragma once

#include <string>
#include <vector>

namespace docx
{

enum class ChartType
{
    Area,                   ///< Диаграмма с областями
    AreaStacked,            ///< Диаграмма с областями и накоплением
    AreaPercentStacked,     ///< Диаграмма нормированная (100%) с областями и накоплением
    Area3D,                 ///< Объемная диаграмма с областями
    Area3DStacked,          ///< Объемная диаграмма с областями и накоплением
    Area3DPercentStacked,   ///< Объемная нормированная (100%) диаграмма с областями и накоплением
    Bar,                    ///< Гистограмма
    BarStacked,             ///< Гистограмма с накоплением
    BarPercentStacked,      ///< Нормированная (100%) гистограмма с накоплением
    Bar3D,                  ///< Объемная гистограмма
    Bar3DStacked,           ///< Объемная гистограмма с накоплением
    Bar3DPercentStacked,    ///< Объемная нормированная (100%) гистограмма с накоплением
    Bubble,                 ///< Пузырьковая диаграмма
    Bubble3D,               ///< Объемная пузырьковая диаграмма
    Column,                 ///< Column chart.
    ColumnStacked,          ///< Stacked Column chart.
    ColumnPercentStacked,   ///< 100\% Stacked Column chart.
    Column3D,               ///< 3D Column chart.
    Column3DStacked,        ///< 3D Stacked Column chart.
    Column3DPercentStacked, ///< 3D 100\% Stacked Column chart.
    Column3DClustered,      ///< 3D Clustered Column chart.
    Doughnut,               ///< Doughnut chart.
    Line,                   ///< Линия
    LineStacked,            ///< График с накоплением
    LinePercentStacked,     ///< Нормированный (100%) график с накоплением
    Line3D,                 ///< Объемная линия
    Pie,                    ///< Pie chart.
    Pie3D,                  ///< 3D Pie chart.
    PieOfBar,               ///< Pie of Bar chart.
    PieOfPie,               ///< Pie of Pie chart.
    Radar,                  ///< Radar chart.
    Scatter,                ///< График точек
    Stock,                  ///< Биржевой график
    Surface,                ///< Surface chart.
    Surface3D               ///< 3D Surface chart.
};

enum class LegendPosition
{
    None,    ///< Без легенды на графике
    Bottom,  ///< Легенда снизу
    Left,    ///< Легенда слева
    Right,   ///< Легенда справа
    Top,     ///< Легенда сверху
    TopRight ///< Легенда справа сверху
};

/// Определяет возможные позиции черточки (деления) значений
enum class AxisTickMark
{
    Cross,   ///< черточки деления по обе стороны от оси
    Inside,  ///< черточки деления вовнутрь графика (от оси)
    Outside, ///< черточки деления вовне графика (от оси)
    None     ///< отсутствуют черточки деления
};

enum Val
{
    Undefined = 7832647
};

struct ChartAxis
{
    std::string name;
    AxisTickMark tickMark = AxisTickMark::None;
    ///Для текстовых координат не имеет смысла
    double majorUnit = Undefined; ///< Интервал основных делений (например, если 5, то деления будут [0, 5, 10, ...]
    double minorUnit = Undefined; ///< Интервал дополнительных делений
    double maxUnit = Undefined;   ///< Верхний предел делений (максимальное на шкале)
    double minUnit = Undefined;   ///< Нижний предел делений (минимальное на шкале)

    unsigned int tickLabelOffset = 50; ///< Расстояние подписей делений от оси (от 0 до 1000)
};

/*!
 * \brief Форма маркера
 */
enum class MarkerSymbol
{
    Default,  ///< По умолчанию
    Circle,   ///< Кружок
    Dash,     ///< Линия
    Diamond,  ///< Алмаз
    Dot,      ///< Точка
    None,     ///< Отсутствие маркера
    Picture,  ///< Картинка
    Plus,     ///< Плюс
    Square,   ///< Квадрат
    Star,     ///< Звезда
    Triangle, ///< Треугольник
    X         ///< Буква Х
};

struct Marker
{
    Marker(MarkerSymbol symbol, int size = 7);
    MarkerSymbol markerSymbol = MarkerSymbol::Default;
    int size = 0; ///< Размер маркера
};

struct LineSettings
{
};

struct Series
{
    Series(std::string name, std::vector<double> xVals, std::vector<double> yVals, Marker marker);
    std::string name;
    std::vector<double> xValues; ///< значения по x
    std::vector<double> yValues; ///< значения по y. Количество должно совпадать с X значениями!!!
    Marker marker;
    LineSettings line;
};

//График
struct Chart
{
    std::string title;
    ChartType chartType = ChartType::Scatter;
    LegendPosition legendPosition = LegendPosition::Bottom;
    std::vector<Series> series;
    ChartAxis xAxis;
    ChartAxis yAxis;
    int width = 480;
    int height = 300;
};

} // namespace docx

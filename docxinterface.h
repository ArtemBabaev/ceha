#pragma once

#include "docxchart.h"
#include "docxenums.h"
#include "docxstruct.h"
#include <functional>
#include <string>
#include <unordered_map>
#include <variant>
#include <vector>

namespace docx
{

class DocImpl;
class ParserController;

using StringList = std::vector<std::string>;
using NumberList = std::vector<double>;

/*!
 * \brief Получить число из строки
 * \param str - строка
 */
double doubleFromString(std::string str);

/*!
 * \brief Получить строку из числа
 * \param val - число
 * \param precision - точность (знаков после запятой)
 */
std::string textVal(double val, int precision = 3);

/*!
 * \brief Удалить повторяющиеся элементы в списке
 * \param vec - список чего-либо
 */
template <class T>
void removeDuplicates(std::vector<T>& vec)
{
    vec.erase(unique(vec.begin(), vec.end()), vec.end());
}

/*!
 * \brief Объект представляет собой одну запись из базы данных
 */
class Entry : public std::unordered_map<std::string, std::string>
{
public:
    /*!
     * \brief Получить число из ячейки по имени колонки. В колонке обязательно должны быть именно числа
     * \param key - имя колонки
     */
    double val(const std::string& key) const;
    /*!
     * \brief Получить текст ячейки по имени колонки.
     * \param key - имя колонки
     */
    std::string text(const std::string& key) const;
};
using EntryList = std::vector<Entry>;

///< Эти штуки надо складывать с текстовкой для получения соответствующего результата,
/// например doc.write("Привет!" + enter);

extern const std::string enter;     ///< Разрыв параграфа (как при нажатии Enter)
extern const std::string pagebreak; ///< Разрыв страницы
extern const std::string linebreak; ///< Перенос строки (как при нажатии Shift + Enter)

/*!
 * \brief Содержит ли строка source подстроку part
 * \param source - исходная строка
 * \param part - искомая подстрока
 */
bool contains(const std::string& source, const std::string& part);

/*!
 * \brief Получить максимальное число из списка чисел
 * \param values - список чисел
 */
double maximum(const NumberList& values);
/*!
 * \brief Получить минимальное число из списка чисел
 * \param values - список чисел
 */
double minimum(const NumberList& values);

/*!
 * \brief Округляет число вверх
 * \example
 * 1.1 -> 2
 * 1.5 -> 2
 * 1.9 -> 2
 */
double roundUp(double val);

/*!
 * \brief Округляет число математически
 * \param count - количество знаков после запятой (доступны отрицательные значения, работает как в Excel)
 * \example
 * 1.1 -> 1
 * 1.5 -> 2
 * 1.9 -> 2
 */
double roundMath(double val, int count = 0);

/*!
 * \brief Округляет число вниз
 * \example
 * 1.1 -> 1
 * 1.5 -> 1
 * 1.9 -> 1
 */
double roundDown(double val);

/*!
 * \brief Получить случайное число из диапазона
 * \param start - первое значение из диапазона
 * \param end - последнее значение из диапазона
 * \param step - шаг между значениями в диапазоне
 */
double randomNum(double start, double end, double step);

class Document
{
public:
    Document(const std::string& folder = "C:\\CEHA\\DOC\\");
    ~Document();
    /*!
     * \brief Пишет в документ текст
     * \param text - тот текст, который будет написан
     *
     * Пишет текст, начиная оттуда, где остался курсор с прошлого вызова.
     * Если вызывается в первый раз, то напишет в самом начале документа
     */
    void write(const std::string& text);

    /*!
     * \brief Завершает построение документа. Вызывать в самом конце
     */
    void endDocument();

    /*!
     * \brief Получить результат вычисления последней написанной формулы
     */
    double lastCalcValue();
    /*!
     * \brief Установить режим записи для формул документа.
     * \param write - режим записи
     *
     * По умолчанию true. Если false - то формулы не будут записываться в документ, но будут исполняться
     * Позволяет унифицировать расчетные участки кода, чтобы они могли быть использованы только для расчетов,
     * например для генерации данных для таблицы или графиков
     */
    bool setFormulaWrite(bool write);

    /*!
     * \brief Выставляет режим разрыва параграфа после каждого вызова doc.write(). По умолчанию true
     * \param val - активность настройки
     * \return Возвращает старое значение настройки
     */
    bool setEnterAfterWrite(bool val);

    /*!
     * \brief Получить случайную строку из списка
     * \param list - список строк
     *
     * Для одного и того же студента (переменная "Студент")
     * случайная строка каждый запуск будет такая же, как и в прошлом запуске
     */
    std::string random(const StringList& list);

    /*!
     * \brief Установить режим режим рандома для конкретной строки
     * \param string - строка
     */
    void setRandomSeed(const std::string& string);

    /*!
     * \brief Зарегистрировать стили в Word
     * \param styles - список стилей
     */
    void registerStyles(const std::vector<Style>& styles);

    void setVar(const std::string& varName, double val); ///< Установить переменной varName значение val
    double var(const std::string& varName);              ///< Получить значение переменной varName
    std::string varText(const std::string& varName);     ///< Получить строку значения переменной varName

    void setStringVar(const std::string& varName, const std::string& val); ///< Установить строковой переменной varName значение val
    std::string stringVar(const std::string& varName);                     ///< Получить строковое значение переменной varName
    bool hasStringVar(const std::string& varName);

    NumberList& Vector(const std::string& varName);      ///< Возвращает список чисел переменной varName
    StringList vectorString(const std::string& varName); ///< Возвращает список чисел, переведенных в строку по переменной varName

    /*!
     * \brief Получить список строк по входному списку чисел
     * \param numberList - список чисел
     */
    StringList vectorString(const NumberList& numberList);

    /*!
     * \brief Начинает построение таблицы
     * \param tableStyle - Стиль, который будет применен ко всему тексту таблицы
     * \param continuationTableActive - Применять ли алгоритм разрыва таблицы и вставки "Продолжение таблицы х.х"
     * Если continuationTableActive=true, то перед началом таблицы обязательно нужно ее подписать, чтобы у нее был уникальный номер
     */
    void startTable(const docx::Style& tableStyle, bool continuationTableActive = true);

    /*!
     * \brief Завершает построение таблицы
     * \param tableAlignment - Выравнивание для всего текста таблицы (из стиля не возьмется, нужно указать здесь)
     */
    void endTable(TextAlignment tableAlignment = AlignLeft);

    /*!
     * \brief Записывает строку в таблице (шапку), состоящую из ячеек, в которых значения headerData (значения шапки)
     * \param headerData - Список строк для вставки в ячейки
     *
     * Эта функция автоматически завершает строку, не нужно после нее вызывать endRow()
     */
    void writeTableHeader(const StringList& headerData);

    /*!
     * \brief Записывает строку в таблице, состоящую из ячеек, в которых значения rowData
     * \param rowData - Список строк для вставки в ячейки
     *
     * Эта функция автоматически завершает строку, не нужно после нее вызывать endRow()
     */
    void writeTableRow(const StringList& rowData, bool wrapWords = true);

    void writePicture(std::string pictureName, docx::Style style);

    /*!
     * \brief Выводит в ворд таблицу, со структурой ячеек, описанной в html и данными из списка строк
     * \param html - код html из сайта https://tablesgenerator.com/html_tables#
     * \param list - список строк, из которых слева направо сверху вниз складывается таблица
     *
     * При составлении таблицы на сайте, нужно нажать галочки "Do not generate CSS", "Compact mode"
     * А также в extra options убрать галку с "First row is a table header"
     */
    void writeHtmlTable(Style contentStyle, const std::string& html, int rowsAsHeader, const StringList& list, bool wrapWords = true, TextAlignment tableAlignment = AlignLeft);

    /*!
     * \brief Написать одну ячейку в текущую строку
     * \param text - Текст ячейки
     * \param wrapWords - (true : переносить слова в ячейке на несколько строк) (false : не переносить слова в ячейке)
     */
    void writeCell(const std::string& text, bool wrapWords = true);

    /*!
     * \brief Написать объединенную ячейку
     * \param text - Текст объединенной ячейки
     * \param columns - количество объединенных ячеек
     */
    void writeMergedCells(const std::string& text, int columns);

    /*!
     * \brief Завершить строку (Использовать в связке с функциями writeCell, writeMergedCells)
     */
    void endRow();

    /*!
     * \brief Устанавливает количество знаков после запятой при конвертации чисел в строки
     * \param precision - количество знаков после запятой
     */
    void setPrecision(int precision);

    /*!
     * \brief Построить список из списка строк
     * \param type - тип списка (маркированный или нумерованный)
     * \param style - Стиль, используемый при построении списка
     * \param items - элементы списка. Между ними будет стандартный параграфный отступ
     */
    void makeList(ListType type, const docx::Style& style, const StringList& items);

    /*!
     * \brief Прочитать переменные из файла и сохранить их для дальнейшего использования
     * \param fileName - имя файла
     */
    void setVariablesFromFile(const std::string& fileName);

    /*!
     * \brief Построить "Список использованной литературы"
     * \param styleName - каким стилем оформить список литературы
     *
     * Создастся список использованной литературы, пронумерованный в порядке использования источников
     */
    void buildSources(const docx::Style& style);
    /*!
     * \brief Добавить источник
     * \param sourceId
     * \param sourceContent
     */
    void addSource(const std::string& sourceId, const std::string& sourceContent);

    /*!
     * \brief Сослаться на источник с идентификатором sourceId
     * \param sourceId - идентификатор источника
     * \param additionalString - то что нужно написать перед закрытием квадратной скобки, например [1,someText]
     */
    std::string source(const std::string& sourceId, const std::string& additionalString = {});
    /*!
     * \brief Сослаться на таблицу с идентификатором tableId
     * \param tableId - идентификатор таблицы
     */
    std::string table(const std::string& tableId);

    /*!
     * \brief Сослаться на рисунок с идентификатором pictureId
     * \param pictureId - идентификатор рисунка
     */
    std::string picture(const std::string& pictureId);

    /*!
     * \brief Сослаться на рисунок с идентификатором pictureId
     * \param pictureId - идентификатор рисунка
     */
    std::string formula(const std::string& formulaId);

    void writeFormula(FormulaSettings settings, Postfix postfix, const std::string& content);
    void writeFormula(FormulaSettings settings, const std::string& content);
    void writeFormula(const std::string& content);

    /*!
     * \brief Добавить график
     * \param chart - График
     */
    void writeChart(const Chart& chart, const Style& style);

    void writeTitle(std::string name);

    /*!
     * \brief Построить содержание
     */
    void writeTableOfContent();

    using Func = std::function<bool(const Entry&)>;
    /*!
     * \brief Осуществляет запрос к базе данных.
     * Получает список всех строк (Entry) из базы, которые удовлетворяют фильтру filter
     * строки в списке лежат в том же порядке, в котором они записаны в базе, сверху вниз.
     *
     * \param tableName - имя таблицы (название файла)
     * \param filter - условие фильтрации
     * \return
     */
    EntryList select(const std::string& tableName, Func&& filter);

private:
    void insertCell(bool wrapWords = false, docx::CellMerge cellMerge = docx::CellMerge::None);

public:
    DocImpl* impl = nullptr;
};

} // namespace docx

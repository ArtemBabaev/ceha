Product {
    id: ceh
    name: "ceha"

    Export {
        Depends {name: "cpp"}
        cpp.includePaths: ceh.sourceDirectory + /../
    }

    Group {
        name: "interface"
        prefix: product.sourceDirectory
        files: ["**/*.h", "**/*.cpp"]
    }
}

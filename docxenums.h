#ifndef DOCXENUMS_H
#define DOCXENUMS_H

namespace docx
{

/*!
 * \brief Ориентация листа
 */
enum Orientation
{
    Book,     ///< Книжная
    Landscape ///< Альбомная
};

/*!
 * \brief Уровень ссылки при построении оглавления
 */
enum ReferenceLevel
{
    NoRef, ///< Без ссылки
    Ref1,  ///< Глава
    Ref2,  ///< подглава
    Ref3,  ///< подподглава
};

enum CellMerge
{
    None = 0,    ///< Ячейка не объединяется
    First = 1,   ///< Первая ячейка в объединении
    Previous = 2 ///< Не первая ячейка в объединении
};

/*!
 * \brief Тип выравнивания текста
 */
enum TextAlignment
{
    AlignLeft,       ///< Выравнивание по левому краю
    AlignCenter,     ///< Выравнивание по центру
    AlignRight,      ///< Выравнивание по правому краю
    AlignAcrossWidth ///< Выравнивание по ширине
};

/*!
 * \brief Способы разрыва строк
 */
enum BreakType
{
    NoBreak,        ///< Ничего не делать
    TextBreak,      ///< Отступ текста (как Shift+Enter)
    ParagraphBreak, ///< Новый параграф (как Enter)
    PageBreak       ///< Разрыв страницы (как Ctrl+Enter)
};

/*!
 * \brief Блоки, на которые разделяется стандартная формула.
 *
 * Используется для настройки видимости частей формулы в итоговой формуле
 * Формулу можно разделить на 4 части, пример этого разбиения ниже:
 * Переменная,  Выражение,    Подстановка, Результат
 * |            |             |            |
 * X =          A + B + C  =  4 + 1 + 2  = 7
 *
 * Видимость этих частей, а также знаков '=' перед ними можно определить в этой структуре
 */
enum FormulaBlocks
{
    Var = 1 << 0,
    Expression = 1 << 1,
    Substitution = 1 << 2,
    Result = 1 << 3,
    AllParts = (1 << 4) - 1
};
using FB = FormulaBlocks;

enum ListType
{
    MarkerListType,
    NumeratedDot,
    NumeratedParen
};

enum Conditions
{
    Or,
    And
};

/*!
 * \brief
 */

enum NumeratedListEnd
{
    Point,
    RightParen
};

} // namespace docx

#endif // DOCXENUMS_H
